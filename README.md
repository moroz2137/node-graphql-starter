# Express と GraphQL の開発環境の構築のガイド

## 1. プロジェクトの紹介

Express は、サーバサイド JavaScript の Web アプリケーションフレームワークです。
このプロジェクトは Express、TypeScript と Apollo Server で作成された Web アプリケーションの開発の雛形です。
以外、サーバーからデータベースへアクセスができます。この雛形のもとにマルツページや API 式アプリケーションの開発が可能。

## 2. 開発環境の構築

先に、外部の依存関係を解決する。Express は Node.js が必要、それに PostgreSQL データベースをインストール。
OS は macOS なら、 `brew` でインストールが可能：

```shell
brew install node postgresql
```

で、Node のライブラリをインストールする。カレントディレクトリで次のコマンドを実行して依存関係を解決する：

```shell
npm install
```

次のコマンドを実行してアセットをビルドする：

```shell
npm run webpack
```

アプリケーションを起動する：

```shell
$ npm start

> graphql_starter@1.0.0 start /Users/karol/node/gql_boilerplate
> npm run build:dev


> graphql_starter@1.0.0 build:dev /Users/karol/node/gql_boilerplate
> nodemon 'src/app.ts' --exec 'ts-node' src/app.ts -e ts,graphql

[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: ts,graphql
[nodemon] starting `ts-node src/app.ts src/app.ts`
Server listening at http://localhost:3000...
```

`http://localhost:3000` にアクセスすると、このページが出れば、開発環境の構築ができました。

`http://localhost:3000/graphql`にアクセスしてブラウザーで GraphQL のテストができます。
![Alt text](/static/graphql-playground.png)
