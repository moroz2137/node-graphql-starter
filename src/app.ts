import express from "express";
import { ApolloServer } from "apollo-server-express";
import path from "path";
import logger from "morgan";
import createError from "http-errors";

import "graphql-import-node";
import * as typeDefs from "./graphql/schema.graphql";
import resolvers from "./graphql/resolvers";
import pageRouter from "./routers/pages";

const gqlServer = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true
});

const app = express();
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "/views"));
app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "../public")));
app.use("/", pageRouter);

gqlServer.applyMiddleware({ app });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.listen(3000, () => {
  console.log(`Server listening at http://localhost:3000...`);
});
