import markdown from "../util/markdown";
import fs from "fs";
import express from "express";
import { promisify } from "util";
import path from "path";

const readFileAsync = promisify(fs.readFile);

const router = express.Router();
const readmePath = path.resolve(__dirname, "../../README.md");

router.get("/", async (req, res, next) => {
  try {
    const readmeMd = await readFileAsync(readmePath);
    const html = markdown(readmeMd.toString());
    res.render("index.html.pug", { html });
  } catch (err) {
    next(err);
  }
});

export default router;
