export interface Product {
  id: string | number;
  name: string;
  basePrice: number;
  description: string;
}
