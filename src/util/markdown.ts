import markdown from "markdown-it";
import highlight from "highlight.js";

highlight.registerLanguage("graphql", <any>function(e: any) {
  return {
    aliases: ["gql"],
    keywords: {
      keyword:
        "query mutation subscription|10 type input schema directive interface union scalar fragment|10 enum on ...",
      literal: "true false null"
    },
    contains: [
      e.HASH_COMMENT_MODE,
      e.QUOTE_STRING_MODE,
      e.NUMBER_MODE,
      {
        className: "type",
        begin: "[^\\w][A-Z][a-z]",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "literal",
        begin: "[^\\w][A-Z][A-Z]",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "variable",
        begin: "\\$",
        end: "\\W",
        excludeEnd: !0
      },
      {
        className: "keyword",
        begin: "[.]{2}",
        end: "\\."
      },
      {
        className: "meta",
        begin: "@",
        end: "\\W",
        excludeEnd: !0
      }
    ],
    illegal: /([;<']|BEGIN)/
  };
});

const renderer = markdown({
  highlight: function(str, lang) {
    if (lang && highlight.getLanguage(lang)) {
      try {
        return highlight.highlight(lang, str).value;
      } catch (__) {}
    }

    return ""; // use external default escaping
  }
});

export default function(md: string) {
  return renderer.render(md);
}
