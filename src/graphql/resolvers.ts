import { IResolvers } from "graphql-tools";
import { Product } from "../models/product";

const sampleProducts: Product[] = [
  {
    id: 1,
    name: "コーヒー",
    basePrice: 400,
    description: "My favorite drink"
  }
];

const resolverMap: IResolvers = {
  Query: {
    hello: (_parent, _args) => {
      return "Hello, world!";
    },
    products: (_parent, _args) => {
      return sampleProducts;
    }
  }
};

export default resolverMap;
