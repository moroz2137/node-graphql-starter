const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"]
  },
  entry: {
    app: path.resolve(__dirname, "assets/js/index.js")
  },
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "js/[name].js"
  },
  module: {
    rules: [
      {
        test: /.(j|t)sx?$/i,
        use: ["babel-loader"]
      },
      {
        test: /.(css|sass|scss)$/i,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          { loader: "css-loader", options: { url: false } },
          "sass-loader"
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: "css/[name].css" }),
    new CopyWebpackPlugin([
      {
        from: "./assets/static",
        to: "./static"
      }
    ])
  ]
};
